/*
 * Para definir nuestra futura libreria
 * Aca se colocan todas las definiciones
 * que se usaran
 *
 */

#ifndef __UTILITIES_H__

    #define __UTILITIES_H__

    #include "postgres.h"

    const char* GetStringFromText(text*);
    text* GetTextFromString(const char*);
    char* GetBufferChar(text*);
    int32 GetLenText(text*);

#endif // __UTITLITIES_H