PG_SERVER_INC= $(shell pg_config --includedir-server)
PG_SERVER_LIB= $(shell pg_config --pkglibdir)
PG_GCC_INC= /usr/lib/x86_64-linux-gnu
PG_LIBS= $(shell pg_config --libs) -lpgfeutils -lpostgres
#PG_LIBS=-lpgcommon -lpgport -lpostgres -lpfeutils
DBNAME=testgo

# export CGO_CFLAGS= -g -fPIC -std=c11 -O2 -D_GNU_SOURCE -I$(PG_SERVER_INC) -L$(PG_SERVER_LIB) $(PG_LIBS) 
export PGPASSWORD=masterkey

MY_LIB_PG=$(abspath out/conversor)
MY_LIB_GO=$(abspath out/conversor.so)

all: $(MY_LIB_GO)
	@echo Compilacion culminada con exito!!!

$(MY_LIB_GO): *.go Makefile
	go build -buildmode=c-shared -o $(MY_LIB_GO)

install: $(MY_LIB_GO)
	psql $(DBNAME) --set=MOD=\'$(MY_LIB_PG)\' -f install.sql 

clean:
	rm -rf out
	go clean -cache
	psql $(DBNAME) -f uninstall.sql 
