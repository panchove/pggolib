/*
 * Cargamos los headers necesarios
 */
#include "postgres.h"
#include "fmgr.h"
#include "utils/builtins.h"

#include "utilities.h"

/*
 * GetTextFromString()
 *
 * Devuelve los datos de un text postgres,
 * tambien sirve para varchar.
 *
 * Parametros:
 *
 *      source -> Puntero tipo text
 *
 * Devuelve:
 *
 *       butter -> puntero que contiene los datos
 */
text* GetTextFromString(const char* source){

    text* buffer;

    buffer = (text*)cstring_to_text(source);

    return buffer;
}
/*
 * GetStringFromText()
 *
 * Devuelve los datos de un text postgres,
 * tambien sirve para varchar.
 *
 * Parametros:
 *
 *      source -> Puntero tipo text
 *
 * Devuelve:
 *
 *       bufferr -> puntero que contiene los datos
 */
const char* GetStringFromText(text* source){

    char* buffer;

    buffer = (char*)text_to_cstring(source);

    return (const char*)buffer;
}
/*
 * GetLenText()
 *
 * Devuelve la longitud de un text postgres,
 * tambien sirve para varchar.
 *
 * Parametros:
 *
 *      source -> Puntero tipo text
 *
 * Devuelve:
 *
 *       len_text -> La longitud del text
 */
int32 GetLenText(text* source){

    int32 len_text;

    len_text = VARSIZE_ANY_EXHDR( source );

    return len_text;
}
/*
 * GetBufferChar()
 *
 * Devuelve un Buffer de la misma longitud de un text postgres,
 * tambien sirve para varchar.
 *
 * Parametros:
 *
 *      source -> Puntero tipo text
 *
 * Devuelve:
 *
 *       buffer -> Puntero al buffer
 */
char* GetBufferChar(text* source){

    char* buffer;

    int32 len_text = GetLenText( source );

    buffer = (char*) palloc( len_text * sizeof(char) );
    MemSet( buffer, '\0', len_text );

    return buffer;
}
