/*
 * Cargamos los headers necesarios
 */
#include "postgres.h"
#include "fmgr.h"
#include "utils/elog.h"
#include "utils/builtins.h"

#include "_cgo_export.h"

PG_FUNCTION_INFO_V1(c_addone);

Datum
c_addone(PG_FUNCTION_ARGS)
{
	int32   arg = PG_GETARG_INT32(0);
	int32 result = goAddOne(arg);
	PG_RETURN_INT32( result );
}
