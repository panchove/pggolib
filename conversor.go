package main

/*
#cgo CFLAGS: -fPIC -std=c11 -D_GNU_SOURCE -I/usr/include/postgresql/12/server
#cgo LDFLAGS: -Wl,-unresolved-symbols=ignore-all -L/usr/lib/postgresql/12/lib
#include "postgres.h"
#include "fmgr.h"

#include "utilities.h"
*/
import "C"

//AddOne adds one to the given arg and retuns it
//export goAddOne
func goAddOne(i C.int32) C.int32 {
	return i + 1
}

//ConcatName concat name and last name
//export goConcatName
func goConcatName(ptrName, ptrLastName *C.char) *C.char {

	var name, lastName string

	//Convertimos los punteros char a string
	name = C.GoString(ptrName)
	lastName = C.GoString(ptrLastName)

	//Devolvemos el puntero
	return C.CString(name + lastName)
}

func main() {}
