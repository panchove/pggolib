DROP FUNCTION IF EXISTS addone(integer);
CREATE OR REPLACE FUNCTION addone(integer)
    RETURNS integer 
    AS :MOD, 'c_addone'
LANGUAGE C STRICT;

DROP FUNCTION IF EXISTS fullname(text, text);
CREATE OR REPLACE FUNCTION fullname(text, text)
    RETURNS text
    AS :MOD,'c_fullname'
LANGUAGE C STRICT;
