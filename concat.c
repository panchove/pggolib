/*
 * Cargamos los headers necesarios
 */
#include "postgres.h"
#include "fmgr.h"
#include "utilities.h"
#include "_cgo_export.h"

/*
 * declaracion de funciones internas no exportables
 */

/*
 * esta funcion concatena dos strings
 * y lo devuelve a postgres
 */
PG_FUNCTION_INFO_V1(c_fullname);

Datum
c_fullname(PG_FUNCTION_ARGS)
{

    /*
     * Recibimos los parametros
     */
    text* ptrName = PG_GETARG_TEXT_PP(0);
    text* ptrLast = PG_GETARG_TEXT_PP(1);

    /*
     * Convertimos de text a string
     */
    const char* name = GetStringFromText( ptrName );
    const char* last = GetStringFromText( ptrLast );

    /*
     * Llamamos a la funcion en GO
     *
     * GoString es mu complicado de manejar
     *
     * Asi que pasamos un punteros a char y que sea Go
     * el que lo transforme a su forma nativa
     *
     * El retorno se debe tratar de igual manera
     * devuelto como un puntero a char
     */
	char* result = goConcatName( (char*)name, (char*)last);

    /*
     * Mostramos el resultado en postgres similar a RAISE INFO
     */
    elog(INFO, "%s", result);

    /*
     * Convertimos a text para ser devuelto a postgres
     */
	text* ptrResult = (text *)GetTextFromString(result);

    PG_RETURN_TEXT_P(ptrResult);

}


